package com.lxch.gulles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GullesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GullesApplication.class, args);
	}
}
