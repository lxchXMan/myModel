package com.lxch.gulles.modular.shrio.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lxch.gulles.modular.shrio.entity.Relation;
import com.lxch.gulles.modular.shrio.mapper.RelationMapper;
import com.lxch.gulles.modular.shrio.service.IRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 */
@Service
public class RelationServiceImpl extends ServiceImpl<RelationMapper, Relation> implements IRelationService {

}
