package com.lxch.gulles.modular.shrio.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lxch.gulles.modular.shrio.entity.Dept;
import com.lxch.gulles.modular.shrio.node.ZTreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * 获取ztree的节点列表
     */
    List<ZTreeNode> tree();

    /**
     * 获取所有部门列表
     */
    List<Map<String, Object>> list(@Param("condition") String condition);

}