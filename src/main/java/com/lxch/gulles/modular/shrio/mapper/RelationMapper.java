package com.lxch.gulles.modular.shrio.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lxch.gulles.modular.shrio.entity.Relation;

/**
 * <p>
  * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 */
public interface RelationMapper extends BaseMapper<Relation> {

}