package com.lxch.gulles.modular.shrio.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lxch.gulles.modular.shrio.entity.Menu;
import com.lxch.gulles.modular.shrio.node.MenuNode;
import com.lxch.gulles.modular.shrio.node.ZTreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 菜单表 Mapper 接口
 * </p>
 *
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据条件查询菜单
     *
     */
    List<Map<String, Object>> selectMenus(@Param("condition") String condition, @Param("level") String level);

    /**
     * 根据条件查询菜单
     *
     */
    List<Long> getMenuIdsByRoleId(@Param("roleId") Integer roleId);

    /**
     * 获取菜单列表树
     *
     */
    List<ZTreeNode> menuTreeList();

    /**
     * 获取菜单列表树
     *
     */
    List<ZTreeNode> menuTreeListByMenuIds(List<Long> menuIds);

    /**
     * 删除menu关联的relation
     *
     * @param menuId
     */
    int deleteRelationByMenu(Long menuId);

    /**
     * 获取资源url通过角色id
     *
     * @param roleId
     */
    List<String> getResUrlsByRoleId(Integer roleId);

    /**
     * 根据角色获取菜单
     *
     * @param roleIds
     */
    List<MenuNode> getMenusByRoleIds(List<Integer> roleIds);
}