package com.lxch.gulles.modular.shrio.service;


import com.baomidou.mybatisplus.service.IService;
import com.lxch.gulles.modular.shrio.entity.Relation;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 */
public interface IRelationService extends IService<Relation> {

}
