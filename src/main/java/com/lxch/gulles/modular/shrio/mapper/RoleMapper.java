package com.lxch.gulles.modular.shrio.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lxch.gulles.modular.shrio.entity.Role;
import com.lxch.gulles.modular.shrio.node.ZTreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 角色表 Mapper 接口
 * </p>
 *
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据条件查询角色列表
     *
     * @return
     */
    public abstract List<Map<String, Object>> selectRoles(@Param("condition") String condition);

    /**
     * 删除某个角色的所有权限
     *
     * @param roleId 角色id
     * @return
     */
    public abstract int deleteRolesById(@Param("roleId") Integer roleId);

    /**
     * 获取角色列表树
     *
     */
    public abstract List<ZTreeNode> roleTreeList();

    /**
     * 获取角色列表树
     *
     * @return
     */
    public abstract List<ZTreeNode> roleTreeListByRoleId(String[] roleId);
}